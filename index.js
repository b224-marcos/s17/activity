/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


*/
	
	//first function here:

	function printWelcomeNote() {
		let fullName = prompt("Enter your full name");
		let age = prompt("Enter your age");
		let location = prompt("Enter your location");

		console.log("Hello, " + fullName)
		console.log("You are " + age + " years old")
		console.log("You live in " + location)
	};

	printWelcomeNote()




/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function getFavoriteBands() {
		console.log("1. Foo Fighters")
		console.log("2. AC / DC")
		console.log("3. Green day")
		console.log("4. Shinedown")
		console.log("5. Evanescence")
	}

	getFavoriteBands()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function getFavoriteMovies() {
		console.log("1. Wakanda Forever")
		console.log("Rotten Tomatoes Rating: 84%")
		console.log("2. Black Adam")
		console.log("Rotten Tomatoes Rating: 40%")
		console.log("3. Black Panther")
		console.log("Rotten Tomatoes Rating: 90%")
		console.log("4. Quiet on the Western Front")
		console.log("Rotten Tomatoes rating: 92%")
		console.log("5. Descendant")
		console.log("Rotten Tomatoes rating: 100%")
	}

	getFavoriteMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


/*console.log(friend1);
console.log(friend2);*/